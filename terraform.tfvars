# Aws
region = "eu-west-1"

# EC2
ami_id             = "ami-02df9ea15c1778c9c"
instance_type      = "t2.micro"
tag_name           = "my-esme-ec2-instance"
aws_public_key_ssh_path = "/mnt/c/Users/willi/.ssh/id_rsa_aws.pub"
aws_private_key_ssh_path = "/mnt/c/Users/willi/.ssh/id_rsa_aws"
